<?php

    class Mahasiswa extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->model('mahasiswa_model', 'm');  
            $this->load->helper('form'); 
            $this->load->helper('url');
            $this->load->model('jurusan_model', 'j');
        }

        public function index()
        {
            $data['judul'] = 'Data Mahasiswa';
            $data['mahasiswa'] = $this->m->getAllmahasiswa();
            $data['jurusan'] = $this->j->getAllJurusan();
            $this->load->view('templates2/header', $data);
            $this->load->view('templates2/sidebar');
            $this->load->view('mahasiswa/datamahasiswa');
            $this->load->view('templates2/footer');
        }

        function ambildata(){
            $data['jurusan'] = $this->j->getAllJurusan(); 
            $datamahasiswa = $this->m->ambildata('datamahasiswa')->result_array();
            echo json_encode($datamahasiswa);
        }

        function tambahdata(){
            $namamahasiswa = $this->input->post('namamahasiswa');
            $nohp = $this->input->post('nohp');
            $jeniskelamin = $this->input->post('jeniskelamin');
            $alamat = $this->input->post('alamat');
            $idjurusan = $this->input->post('idjurusan');
            

            if($namamahasiswa==''){
                $result['pesan']="Nama mahasiswa harus diisi";
            }else{
                $result['pesan']="";

            $data=array(
                'namamahasiswa' => $namamahasiswa,
                'nohp' => $nohp,
                'jeniskelamin' => $jeniskelamin,
                'alamat' => $alamat,
                'idjurusan' => $idjurusan,
                
            );

            $this->m->tambahdata($data,'datamahasiswa');

            }

            echo json_encode($result);
        }

        public function ambilidmahasiswa()
        {
            $idmahasiswa=$this->input->post('idmahasiswa');
            $where=array('idmahasiswa' => $idmahasiswa);
            $datamahasiswa = $this->m->ambilidmahasiswa('datamahasiswa', $where)->result();

            echo json_encode($datamahasiswa);
        }

        public function ubahdata()
        {
            $idmahasiswa = $this->input->post('idmahasiswa');
            $namamahasiswa = $this->input->post('namamahasiswa');
            $nohp = $this->input->post('nohp');
            $jeniskelamin = $this->input->post('jeniskelamin');
            $alamat = $this->input->post('alamat');
            $idjurusan = $this->input->post('idjurusan');
            

            if($namamahasiswa==''){
                $result['pesan']="Nama mahasiswa harus diisi";
            }else{
                $result['pesan']="";

            $where=array('idmahasiswa'=>$idmahasiswa);

            $data=array(
                'namamahasiswa' => $namamahasiswa,
                'nohp' => $nohp,
                'jeniskelamin' => $jeniskelamin,
                'alamat' => $alamat,
                'idjurusan' => $idjurusan,
                
            );

            $this->m->updatedata($where,$data,'datamahasiswa');

            }

            echo json_encode($result);
        }

        public function ambiliddetail()
        {
            $data['jurusan'] = $this->j->getAllJurusan();
            $idmahasiswa=$this->input->post('idmahasiswa');
            $where=array('idmahasiswa' => $idmahasiswa);
            $datamahasiswa = $this->m->ambiliddetail('datamahasiswa', $where)->result();

            echo json_encode($datamahasiswa);
        }


        public function hapusdata()
        {
            $idmahasiswa=$this->input->post('idmahasiswa');
            $where=array('idmahasiswa'=>$idmahasiswa);

            $this->m->hapusdata($where,'datamahasiswa');
        }

        public function print(){
            $data['mahasiswa'] = $this->m->ambildata("datamahsiswa")->result();
            $this->load->view('mahasiswa/printmahasiswa', $data);
        }

        public function pdf(){
            $this->load->library('dompdf_gen');

            $data['mahasiswa'] = $this->m->ambildata("datamahasiswa")->result();
            $this->load->view('mahasiswa/pdf', $data);

            $paper_size = 'A4';
            $orientation = 'landscape';
            $html = $this->output->get_output();
            $this->dompdf->set_paper($paper_size, $orientation);

            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("data_mahasiswa.pdf", array('Attachement' =>0));
        }

        public function excel(){
            $data['mahasiswa'] = $this->m->ambildata("datamahsiswa")->result();

            require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
            require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
            ->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Data Mahasiswa')
                 ->setLastModifiedBy('Data Mahasiswa')
                 ->setTitle("Data Mahasiswa")
                 ->setSubject("Data Mahasiswa")
                 ->setDescription("Data Mahasiswa")
                 ->setKeywords("Data Mahasiswa");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );

    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
    $objDrawing->setCoordinates('A1');
    $objDrawing->setName('univ');
    $objDrawing->setDescription('univ');
    $objDrawing->setPath('asset/gambar/univ.png');
    $objDrawing->setWidth(100)->setHeight(100);


    $excel->setActiveSheetIndex(0)->setCellValue('A2',"Data Mahasiswa Universitas Surakarta"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A2:E2')
    ; // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

    $excel->setActiveSheetIndex(0)->setCellValue('A4', "Tahun 2020/2021"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A4:E4'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A4')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A6', "Nama Mahasiswa"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B6', "No  Telepon"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('C6', "Jenis Kelamin"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('D6', "Alamat"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('E6', "Jurusan"); // Set kolom B3 dengan tulisan "NIS"


    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);

    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
    //$jurusan = $this->m->view('jurusan/datajurusan');
    //$no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 7; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($data['mahasiswa'] as $jrs){// Lakukan looping pada variabel siswa

      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $jrs->namamahasiswa);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $jrs->nohp);      
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $jrs->jeniskelamin);      
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $jrs->alamat);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $jrs->jurusan);

      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);

    //   $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      
      //$no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(25); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Mahasiswa");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Mahasiswa.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');

        }


    }