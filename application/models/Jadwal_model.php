<?php

    class Jadwal_model extends CI_model {

        public function getAllJadwal()
        {
            $this->db->select('*');
            $this->db->from('datajadwal');
            $this->db->join('datasemester','datasemester.idsemester = datajadwal.idsemester');
            $this->db->join('datamatkul','datamatkul.idmatkul = datajadwal.idmatkul');
            return $query = $this->db->get()->result_array();
        }


        function ambildata(){
            //return $this->db->get('datamahasiswa');
            $this->db->select('*');
            $this->db->from('datajadwal');
            $this->db->join('datasemester','datasemester.idsemester = datajadwal.idsemester');
            $this->db->join('datamatkul','datamatkul.idmatkul = datajadwal.idmatkul');
            return $query = $this->db->get();
        }

        public function tambahdata($data, $table)
        {
            $this->db->insert($table, $data);
        }

        public function ambilidjadwal($table, $where)
        {
            return $this->db->get_where($table, $where);
        }

        public function updatedata($where,$data,$table)
        {
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        // public function ambiliddetail($table, $where)
        // {
        //     return $this->db->get_where($table, $where);
        // }


        public function hapusdata($where,$table)
        {
            $this->db->where($where);
            $this->db->delete($table);
        }

    }

?>