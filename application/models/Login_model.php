<?php

    class Login_model extends CI_model {

        public function getAllLogin()
        {
            return $query = $this->db->get('datalogin')->result_array();
        }
    
        public function ambillogin($namalogin,$passlogin)
        {
         $this->db->where('namalogin', $namalogin);
         $this->db->where('passlogin', $passlogin);
         $query = $this->db->get('datalogin');
         if ($query->num_rows()>0){
          foreach ($query->result() as $row) {
           $sess = array ('namalogin' => $row->namalogin,
               'passlogin' => $row->passlogin
             );
          }
         $this->session->set_userdata($sess);
         redirect('Home');
         }
         else{
          $this->session->set_flashdata('info','MAAF Username dan Password Anda salah!, Mohon diperiksa kembali');
          redirect('Login');
         }
        }
        
        public function keamanan(){
            $namalogin = $this->session->userdata('namalogin');
            if(empty($namalogin)){
             $this->session->sess_destroy();
             redirect('login');
            }
           }


    }
?>