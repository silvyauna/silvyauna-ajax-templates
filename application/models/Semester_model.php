<?php

    class Semester_model extends CI_model {

        public function getAllSemester()
        {
            return $query = $this->db->get('datasemester')->result_array();
        }


        function ambildata(){
            return $this->db->get('datasemester');
        }

        public function tambahdata($data, $table)
        {
            $this->db->insert($table, $data);
        }

        public function ambilidsemester($table, $where)
        {
            return $this->db->get_where($table, $where);
        }

        public function updatedata($where,$data,$table)
        {
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        public function ambiliddetail($table, $where)
        {
            return $this->db->get_where($table, $where);
        }


        public function hapusdata($where,$table)
        {
            $this->db->where($where);
            $this->db->delete($table);
        }

    }

?>