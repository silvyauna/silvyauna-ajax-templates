<?php

    class Matkul_model extends CI_model {

        public function getAllMatkul()
        {
            $this->db->select('datamatkul.idmatkul,datamatkul.namamatkul,datasemester.namasemester');
            $this->db->from('datamatkul');
            $this->db->join('datasemester','datamatkul.idsemester = datasemester.idsemester');
            return $query = $this->db->get()->result_array();
        }


        function ambildata(){
            //return $this->db->get('datamahasiswa');
            $this->db->select('datamatkul.idmatkul,datamatkul.namamatkul,datasemester.namasemester');
            $this->db->from('datamatkul');
            $this->db->join('datasemester','datamatkul.idsemester = datasemester.idsemester');
            return $query = $this->db->get();
        }

        public function tambahdata($data, $table)
        {
            $this->db->insert($table, $data);
        }

        public function ambilidmatkul($table, $where)
        {
            return $this->db->get_where($table, $where);
        }

        public function updatedata($where,$data,$table)
        {
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        public function ambiliddetail($table, $where)
        {
            return $this->db->get_where($table, $where);
        }


        public function hapusdata($where,$table)
        {
            $this->db->where($where);
            $this->db->delete($table);
        }

    }

?>