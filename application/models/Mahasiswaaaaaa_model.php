<?php

    class Mahasiswa_model extends CI_model {

        public function getAllMahasiswa()
        {
            return $query = $this->db->get('datamahasiswa')->result_array();
        }

        public function tambahDataMahasiswa($data, $table)
        {
            $this->db->insert($table, $data);
        }

        public function edit_mahasiswa($where)
        {
            return $this->db->get_where('datamahasiswa', $where);
        }

        public function ubah($where, $data)
        {
            $this->db->where($where);
            $this->db->update('datamahasiswa', $data);
        }   

        public function hapusDataMahasiswa($where)
        {
            $this->db->where($where);
            $this->db->delete('datamahasiswa');
        }

    }