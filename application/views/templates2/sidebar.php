
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>asset/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Faustina Chelloana</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu</li>

        <li class="">
          <a href="<?php echo base_url(); ?>Home" class="nav-link active">
            <i class="fa fa-home"></i> <span>Home</span>         
          </a>         
        </li>

        <li class="">
          <a href="<?php echo base_url(); ?>Mahasiswa" class="nav-link active">
            <i class="fa fa-graduation-cap"></i> <span>Mahasiswa</span>         
          </a>         
        </li>
        <li class="">
          <a href="<?php echo site_url();?>Jurusan" class="nav-link">
            <i class="fa fa-university"></i> <span>Jurusan</span>         
          </a>
        </li>
        <li class="">
          <a href="<?php echo site_url();?>Semester" class="nav-link">
            <i class="fa fa-user"></i> <span>Semester</span>
          </a>
        </li>
        <li class="">
          <a href="<?php echo site_url();?>Matkul" class="nav-link">
            <i class="fa fa-book"></i> <span>Mata Kuliah</span>
          </a>
        </li>
        <li class="">
          <a href="<?php echo site_url();?>Jadwal" class="nav-link">
            <i class="fa fa-list"></i> <span>Jadwal</span>
          </a>
        </li>
        <li class="">
          <a href="<?php echo site_url();?>Login" class="nav-link">
            <i class="fa fa-sign-out"></i> <span>Logout</span>
          </a>
        </li>
      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>