<div class="divcontainer">

    <div class="divrow mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Form Tambah Data Jurusan
            </div>

            <div class="card-body">

                <form action="<?= base_url(); ?>Jurusan/simpan" method="post">

                    <div class="form-group">
                        <label for="jurusan">Nama Jurusan</label>
                        <input type="text" name="jurusan" class="form-control" id="jurusan">
                    </div>

                    <button type="submit" name="tambah" class="btn btn-success">Tambah</button>

                </form>

            </div>
        </div>
        
           
        </div>
    </div>

</div>