<div class="contaier">

    <h1>Data Mahasiswa</h1>

    <br>

    <div class="row">
        <div class="col-md-6">
            <a href="<?= base_url(); ?>mahasiswa/tambahmahasiswa" class="btn btn-primary">Tambah Data Mahasiswa</a>
        </div>
    </div>

    <br>

    <table class="table">

    

        <thead class="thead-dark">
            <tr>
                <th scope="col">ID Mahasiswa</th>
                <th scope="col">Nama Mahasiswa</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Alamat</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>

        <tbody>

        <?php foreach ($mahasiswa as $mhs) : ?>

            <tr>
                <td><?= $mhs['idmahasiswa'] ?></td>
                <td><?= $mhs['namamahasiswa'] ?></td>
                <td><?= $mhs['jeniskelamin'] ?></td>
                <td><?= $mhs['alamat'] ?></td>
                <td><a href='<?= base_url(); ?>mahasiswa/editmahasiswa/<?= $mhs['idmahasiswa']; ?>' class="btn btn-warning">Edit</a>
                    <a href='<?= base_url(); ?>mahasiswa/hapusmahasiswa/<?= $mhs['idmahasiswa']; ?>' class="btn btn-danger"
                    onclick="return confirm('Hapus Data?')"> Hapus</a>
                </td>
            </tr>

        <?php endforeach; ?>

        </tbody>

    </table>

</div>