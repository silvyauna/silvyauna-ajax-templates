<div class="divcontainer">

    <div class="divrow mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Form Edit Data Mahasiswa
            </div>

            <?php foreach ($mahasiswa as $mhs) { ?>

            <div class="card-body">


                <form action="<?= base_url().'Mahasiswa/ubah' ?>" method="post">

                    <div class="form-group">
                        <label for="namamahasiswa">Nama Mahasiswa</label>
                        <input type="hidden" name="idmahasiswa" class="form-control" value="<?= $mhs->idmahasiswa ?>">
                        <input type="text" name="namamahasiswa" class="form-control" value="<?= $mhs->namamahasiswa ?>" id="namamahasiswa">
                    </div>

                    <div class="form-group">
                        <label for="jeniskelamin">Jenis Kelamin</label>                       
                        <select name="jeniskelamin" class="form-control">

                                <option value="<?= $mhs->jeniskelamin?>"
                                    <?php $kondisi = ($mhs->jeniskelamin);
                                        if ($kondisi == "L"){
                                            echo "selected";
                                        }
                                    
                                ?>> Laki-Laki </option>

                                <option value="<?= $mhs->jeniskelamin?>"
                                    <?php $kondisi = ($mhs->jeniskelamin);
                                        if ($kondisi == "P"){
                                            echo "selected";
                                        }
                                    
                                ?>> Perempuan </option>

                                
                        </select>
                    </div>
                
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="hidden" name="idmahasiswa" class="form-control" value="<?= $mhs->idmahasiswa ?>">
                        <input type="text" name="alamat" value="<?= $mhs->alamat ?>" class="form-control" id="alamat">
                    </div>

                    <button type="submit" name="tambah" class="btn btn-success">Edit</button>

                </form>

            <?php } ?>


            </div>
        </div>
        
           
        </div>
    </div>

</div>